import React from 'react';
import './PremiumUI.css';

const PremiumCardTop = props =>{
    return(
        <li className='PremiumCard'>
            <div className='Top'>
                <div className='Header'>
                    <p style={{width:150, backgroundColor:'white'}} className='info'>Bayar satu kali</p>
                    <h3>Mini</h3>
                    <p>Mulai Rp 2500/hari
                        <br/>
                        1 akun
                    </p>
                </div>
            </div>
        </li>
    );
}

export default PremiumCardTop;