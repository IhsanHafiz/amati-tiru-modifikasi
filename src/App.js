import logo from './logo.svg';
import './App.css';
import React, { Component } from 'react';
import Navigasibar from './Navbar.js';
import './Body.css';
import './Cards.css';
import Card from './CardUI.js'
import Benefit1 from './Images/benefit_1.png';
import Benefit2 from './Images/Benefit_2.png';
import Benefit3 from './Images/benefit_3.png';
import Benefit4 from './Images/benefit_4.png';
import PremiumCardTop from './PremiumUITop.js'
import './PremiumUI.css';
import { AiOutlineCheck }  from "react-icons/ai";
import PremiumCardUI from './PremiumCardUI.js'
import FooterSection from './Footer.js';

function Header() {
  return(
    <div className='Header'>
      <div className="HeaderWrapper">
        <h1>Memperkenalkan Premium Mini</h1>
        <h2>
          Rp 2500 untuk 1 hari.
          <br/>
          Rp 9500 untuk 1 minggu.
        </h2> 
        <div className="ButtonWrapper">
          <button className='Black'>DAPATKAN MINI</button>
          <button className='Transparent'>LIHAT PAKET LAINNYA</button>
        </div>
        <footer>Persyaratan dan ketentuan berlaku</footer>   
      </div>
    </div>
  );
}

function Body(){
  return(
    <div className='Body'>
    <div className="BodyWrapper">
      <h1>Dapatkan Premium Family</h1>
      <h2>
       Anggota keluarga yang tinggal serumah bisa menikmati hingga 6 akun Premium.
        <br/>
        <br/>
        <b>Berlangganan atau bayar sekali, mulai dari Rp 79000/bulan.</b>
      </h2> 
      <div className="ButtonWrapper">
        <button className='White'>DAPATKAN PREMIUM FAMILY</button>
      </div>
    </div>
  </div>
  );
}

class Cards extends Component{
  render(){
    return(
      <div className='CardBody'>
        <div className='CardSection'>
          <header>
            <h1>Mengapa beralih ke Premium?</h1>
          </header>
          <ul className='CardWrapper'>
            <Card imagesrc={Benefit1} title='Download mmusik.' desc='Dengarkan di mana saja.'/>
            <Card imagesrc={Benefit2} title='Tidak ada gangguan iklan.' desc='Nikmati musik tanpa gangguan.'/>
            <Card imagesrc={Benefit3} title='Mainkan lagu apa saja.' desc='Bahkan di ponsel.'/>
            <Card imagesrc={Benefit4} title='Lewati tanpa batas.' desc='Cukup tekan berikutnya.'/>
          </ul>
        </div>
      </div>
    );
  }
}

class PremiumCards extends Component{
  render(){
    return(
      <div className='PremiumBody'>
        <div className='PremiumSection'>
          <header>
            <h1>Pilih Premium Kamu</h1>
            <h3>Pilih paket untuk melihat semua tawaran, durasi, dan opsi pembayaran.</h3>
          </header>
          <ul className='PremiumWrapper'>
            <div>
              <PremiumCardTop/>
              <div className='Middle'>
                <div className='Desc'>
                  <p><AiOutlineCheck/> Dengarkan musik bebas iklan di ponsel</p>
                  <p><AiOutlineCheck/> Lewati lagu tanpa batas</p>
                  <p><AiOutlineCheck/> Group Session</p>
                  <p><AiOutlineCheck/> Download 30 lagu di 1 perangkat seluler</p>
                  <div className='PremiumButtonWrapper'>
                  <button className='PremiumButton' style={{marginTop:210}}>LIHAT PAKET</button>
                  </div>
                  <p className='SdK'>Persyaratan dan ketentuan berlaku.</p>
                </div> 
              </div>
            </div>
            <div>
              <PremiumCardUI jenis='Individual' harga1='Penawaran dari Rp 49990 / bulan' akun='1 akun'/>
                <div className='Middle'>
                  <div className='Desc'>
                    <p><AiOutlineCheck/> Dengarkan musik bebas iklan</p>
                    <p><AiOutlineCheck/> Putar dalam urutan apa pun</p>
                    <p><AiOutlineCheck/> Lewati lagu tanpa batas</p>
                    <p><AiOutlineCheck/> Group Session</p>
                    <p><AiOutlineCheck/> Download 10.000 lagu/perangkat, hingga 5 perangkat per akun</p>
                    <div className='PremiumButtonWrapper'>
                    <button className='PremiumButton' style={{marginTop:168}}>LIHAT PAKET</button>
                    </div>
                    <p className='SdK'>Persyaratan dan ketentuan berlaku.</p>
                  </div> 
                </div>
            </div>
            <div>
              <PremiumCardUI jenis='Duo' harga1='Penawaran dari Rp 64990 / bulan' akun='2 akun'/>
                <div className='Middle'>
                  <div className='Desc'>
                    <p><AiOutlineCheck/> Untuk pasangan yang tinggal serumah</p>
                    <p><AiOutlineCheck/> Dengarkan musik bebas iklan</p>
                    <p><AiOutlineCheck/> Putar dalam urutan apa pun</p>
                    <p><AiOutlineCheck/> Lewati lagu tanpa batas</p>
                    <p><AiOutlineCheck/> Group Session</p>
                    <p><AiOutlineCheck/> Download 10.000 lagu/perangkat, hingga 5 perangkat per akun</p>
                    <div className='PremiumButtonWrapper'>
                    <button className='PremiumButton' style={{marginTop:105}}>LIHAT PAKET</button>
                    </div>
                    <p className='SdK'>Persyaratan dan ketentuan berlaku.</p>
                  </div> 
                </div>
            </div>
            <div>
              <PremiumCardUI jenis='Duo' harga1='Penawaran dari Rp 64990 / bulan' akun='2 akun'/>
                <div className='Middle'>
                  <div className='Desc'>
                    <p><AiOutlineCheck/> Untuk keluarga yang tinggal serumah</p>
                    <p><AiOutlineCheck/> Blokir musik explicit</p>
                    <p><AiOutlineCheck/> Untuk pasangan yang tinggal serumah</p>
                    <p><AiOutlineCheck/> Dengarkan musik bebas iklan</p>
                    <p><AiOutlineCheck/> Putar dalam urutan apa pun</p>
                    <p><AiOutlineCheck/> Lewati lagu tanpa batas</p>
                    <p><AiOutlineCheck/> Group Session</p>
                    <p><AiOutlineCheck/> Download 10.000 lagu/perangkat, hingga 5 perangkat per akun</p>
                    <div className='PremiumButtonWrapper'>
                    <button className='PremiumButton'>LIHAT PAKET</button>
                    </div>
                    <p className='SdK'>Persyaratan dan ketentuan berlaku.</p>
                  </div> 
                </div>
            </div>
          </ul>
          <div className='Diskon'>
            <div className='DiskonWrapper'>
              <h3>Diskon khusus untuk pelajar di tingkat universitas yang memenuhi syarat.</h3>
              <button className='DiskonButton'>
              PERIKSA 
              <br/>
              KELAYAKAN</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}


function App() {
  return (
    <div>
      <Navigasibar/>
      <Header/>
      <Body/>
      <Cards/>
      <PremiumCards/>
      <FooterSection/>
    </div>
  );
}

export default App;
