import React from 'react';
import './CardUI.css';
import Benefit1 from './Images/benefit_1.png';

const Card = props =>{
    return(
        <li className='Card'>
            <img src={props.imagesrc} />
            <div className='DescWrapper'>
                <p classname='Title' style={{fontWeight:'bold', marginBottom:10, fontSize:21}}>{props.title}</p>
                <p className='Desc2' style={{fontSize:16, marginLeft:21, marginRight:21, marginBottom:0}}>{props.desc}</p>
            </div>
        </li>
    );
}

export default Card;