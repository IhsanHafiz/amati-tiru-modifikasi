import React, { useState } from 'react';
import './Navbar.css';
import Spotify from './Images/method-draw-image Black.svg';

const Navigasibar = () => {
    return(
        <div className='Navbar'>
             <div className='BrandWrapper'><img src={Spotify}/></div>
            <div className='NavbarMenu'>
                <a style={{justifyContent:'flex-end'}}>Premium</a>
                <a>Dukungan</a>
                <a>Download</a>
                <div className='Vertical'></div>
                <a>Daftar</a>
                <a>Masuk</a>
            </div>
        </div>
    );
}

export default Navigasibar;