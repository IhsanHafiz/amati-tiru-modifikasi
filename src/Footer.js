import React from 'react';
import './FooterStyle.css';
import Logo from './Images/method-draw-image White.svg'
import { RiInstagramLine } from "react-icons/ri";
import {FaTwitter} from "react-icons/fa";
import {FaFacebookF} from 'react-icons/fa';
import Indonesia from './Images/Indonesia.png';

function FooterSection(){
    return(
        <div classNam='Footer' style={{backgroundColor:'black',paddingTop:80, paddingBottom:50, display:'block'}}>
            <div className='FooterWrapper' style={{margin:'0px 47px', display:'flex'}}>
                <div className='Wrapper' style={{height:346, width:195, padding: '0px 15px', margin:0}}>
                    <img src={Logo} style={{width:171, height:60}}/>
                </div>
                <div className='Wrapper' style={{height:346, width:195, padding: '0px 15px', margin:0, display:'block', justifyContent:'flex-start'}}>
                    <p style={{fontSize:16, margin:'20px 0px', color:'rgb(145, 148, 150)', fontWeight:900, lineHeight:1.4}}>PERUSAHAAN</p>
                    <a style={{cursor:'pointer', fontSize:16, color:'white', padding: '3px 0px 15px'}}>Tentang</a>
                    <br/>
                    <a style={{cursor:'pointer', fontSize:16, color:'white', padding: '3px 0px 15px'}}>Pekerjaan</a>
                    <br/>
                    <a style={{cursor:'pointer', fontSize:16, color:'white', padding: '3px 0px 15px'}}>For the Record</a>
                </div>
                <div className='Wrapper' style={{height:346, width:195, padding: '0px 15px', margin:0, display:'block', justifyContent:'flex-start'}}>
                    <p style={{fontSize:16, margin:'20px 0px', color:'rgb(145, 148, 150)', fontWeight:900, lineHeight:1.4}}>KOMUNITAS</p>
                    <a style={{cursor:'pointer', fontSize:16, color:'white', padding: '3px 0px 15px'}}>Untuk Artis</a>
                    <br/>
                    <a style={{cursor:'pointer', fontSize:16, color:'white', padding: '3px 0px 15px'}}>Pengembang</a>
                    <br/>
                    <a style={{cursor:'pointer', fontSize:16, color:'white', padding: '3px 0px 15px'}}>Iklan</a>
                    <br/>
                    <a style={{cursor:'pointer', fontSize:16, color:'white', padding: '3px 0px 15px'}}>Investor</a>
                    <br/>
                    <a style={{cursor:'pointer', fontSize:16, color:'white', padding: '3px 0px 15px'}}>Vendor</a>
                </div>
                <div className='Wrapper' style={{height:346, width:195, padding: '0px 15px', margin:0, display:'block', justifyContent:'flex-start'}}>
                    <p style={{fontSize:16, margin:'20px 0px', color:'rgb(145, 148, 150)', fontWeight:900, lineHeight:1.4}}>TAUTAN BERGUNA</p>
                    <a style={{cursor:'pointer', fontSize:16, color:'white', padding: '3px 0px 15px'}}>Dukungan</a>
                    <br/>
                    <a style={{cursor:'pointer', fontSize:16, color:'white', padding: '3px 0px 15px'}}>Pemutar Web</a>
                    <br/>
                    <a style={{cursor:'pointer', fontSize:16, color:'white', padding: '3px 0px 15px'}}>Aplikasi Seluler Gratis</a>
                </div>
                <div className='SocialMediaSection'>
                    <a><RiInstagramLine className='SocialMediaIcon'/></a>
                    <a><FaTwitter className='SocialMediaIcon'/></a>
                    <a><FaFacebookF className="SocialMediaIcon"/></a>
                </div>
            </div>
            <div style={{margin:'0px 47px'}}>
                    <div className='Country'>
                        <a style={{color:'#9191496'}}>Indonesia
                        <img src={Indonesia}/>
                        </a>
                    </div>
            </div>
            <div style={{display:'flex',margin:'0px 47px'}}>
                <div className="Policy">
                    <a>Hukum</a>
                    <a>Pusat Privasi</a>
                    <a>Kebijakan Privasi</a>
                    <a>Cookies</a>
                    <a>Tentang Iklan</a>
                </div>
                <div className='Copyright'>
                <footer>&copy; 2021 Spotify AB</footer>
                </div>
            </div>
        </div>
    );
}

export default FooterSection;