import React from 'react';
import './PremiumUI.css';

const PremiumCardUI = props =>{
    return(
        <li className='PremiumCard'>
            <div className='Top'>
                <div className='Header'>
                    <p style={{width:200, backgroundColor:'#2e77d0', marginBottom:10, color:'white', fontSize:10}} className='info'>Berlangganan, gratis 1 bulan</p>
                    <p style={{width:150, backgroundColor:'white'}} className='info'>Bayar satu kali</p>
                    <h3>{props.jenis}</h3>
                    <p>{props.harga1}
                        <br/>
                        {props.akun}
                    </p>
                </div>
            </div>
        </li>
    );
}

export default PremiumCardUI;